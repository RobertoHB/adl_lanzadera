-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Servidor: db5000652008.hosting-data.io
-- Tiempo de generación: 10-02-2021 a las 18:03:23
-- Versión del servidor: 5.7.32-log
-- Versión de PHP: 7.0.33-0+deb9u10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dbs607010`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preregistro`
--

CREATE TABLE `preregistro` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellidos` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `talentos`
--

CREATE TABLE `talentos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `apellidos` varchar(200) DEFAULT NULL,
  `profesion` varchar(300) DEFAULT NULL,
  `tipo` varchar(100) DEFAULT NULL,
  `empresa` tinyint(1) NOT NULL,
  `particular` tinyint(1) NOT NULL,
  `lanzadera` varchar(100) DEFAULT NULL,
  `telefono` int(11) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `web` varchar(200) DEFAULT NULL,
  `linkedin` varchar(200) DEFAULT NULL,
  `foto` varchar(100) DEFAULT NULL,
  `sobremi` text,
  `carta` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `talentos`
--

INSERT INTO `talentos` (`id`, `nombre`, `apellidos`, `profesion`, `tipo`, `empresa`, `particular`, `lanzadera`, `telefono`, `email`, `web`, `linkedin`, `foto`, `sobremi`, `carta`) VALUES
(1, 'Begoña', 'Méndez Fernández', 'Dibujo e ilustración tradicional y digital Diseño de estampados para papelería Diseño de patterns Decoración de producto', 'Particular', 0, 1, 'Medio Cudeyo', 625476668, 'mendezilustración@gmail.com', '', '', '', 'Los recuerdos de mi infancia se asocian al color, a las manchas de pintura en mis manos\r\ny al intenso olor de la trementina que aún está en mi memoria. Soy una apasionada del\r\ndibujo, ese que crece frente a una ventana, con un café en una mano y un lápiz en la otra\r\npermitiendome colorear la vida como yo la veo.\r\n', 'Desarrollé la mayor parte de mi actividad laboral en el ámbito de la rotulación, el diseño y la serigrafía textil, trabajando tanto por cuenta ajena como propia, pero después de 21 años de duro trabajo como autónoma en el sector, doy un giro a mis expectativas laborales para mi presente y mi futuro inmediato, retomando mis pinceles e intentando encontrar oportunidades en el mundo del dibujo  y la ilustración profesional.\r\n\r\nSoy una persona creativa, honesta y trabajadora, no me asusta el trabajo duro, pero adoro la libertad que me proporciona gestionar mi propio tiempo, esto hace que me sienta más productiva  y concentrada.\r\n\r\nMe siento muy afortunada por que la vida me haya devuelto al oficio que nunca debí dejar atrás, que me renueva la ilusión de los nuevos proyectos y por supuesto , me permite seguir amando lo que hago.'),
(2, 'David', 'Carretero Blake', 'Fotógrafo Especializado en Arquitectura y Publicidad', 'Particular', 1, 1, 'Medio Cudeyo', 653921367, 'hola@davidcarretero.net', 'https://www.davidcarretero.net', 'https://www.linkedin.com/in/davidcarreterophoto', NULL, 'Como fotógrafo me dedico a transformar lo negativo en\r\npositivo y a poner el foco en lo que es realmente importante.', 'Descubrí la fotografía muy de niño durante un viaje con mi madre y con una cámara fisher price de\r\ncolores. La historia tiene su guasa, pero te la cuento mejor en persona. La cosa es que allí nació\r\nuna apreciación por la luz que unida a mi curiosidad me puso definitivamente en la senda de la\r\nfotografía por la que he transitado cada día desde entonces.\r\n20+ años trabajando como director de fotografía, primero en cine y actualmente rodando\r\npublicidad por todo el mundo, especializado en marcas cosméticas como Pantene, L´Óreal,\r\nMaybelline, H&S… Me ha regalado la posibilidad de perderme y fotografiar en mis ratos libres allí\r\ndonde estuviera: Milan, Praga, Bucharest, Caracas, Shanghai, Bangkok, Líbano. Nunca me aburro\r\n(aunque alguna vez me quedo sin película…).\r\nLa experiencia me ha demostrado que lo mejor para la creatividad es tener limitaciones con las que\r\ntrabajar. Que a lo que se te pone en contra en un proyecto se le puede dar la vuelta y convertirlo en\r\nvirtud y que el accesorio mas importante para tu cámara es un buen par de zapatos cómodos.'),
(6, 'Marta', 'Ruiz Fernández', 'Ventas Atención al Público Personal Shopper Línea de Producción', 'Particular', 0, 1, 'Medio Cudeyo', 685447401, 'martaruizfernandez851@gmail.com', '', '', NULL, 'Soy Marta, una persona segura de mí misma, podría definirme\r\ncomo constructora de confianza, por lo que hago posible que mis\r\nclientes se convierten en los héroes de mi propia historia….', 'Como especialista en ventas. mi etiqueta es trasmitir confianza y seguridad, por lo que se me\r\nhace más sencillo conseguir buenos resultados, fruto también de mis capacidades profesionales\r\ny de mi buena actitud ante los desafíos que se me presentan en la vida…. Diría que un producto,\r\nun sector y un argumento, se aprende fácil, la ACTITUD, se tiene o no se tiene….\r\nMe gusta cumplir con mis obligaciones y me esfuerzo por aprender de cada situación…\r\nLo que más me gusta es la venta personalizada, la que aporta soluciones al cliente. Soy una\r\npersona polivalente, resolutiva, implicada, positiva y amante de los retos tanto en lo personal\r\ncomo en lo profesional, por eso mi vida es una constante venta…\r\n\"Amo las ventas y las ventas me aman a mi\"\r\nSi desea contactar conmigo, podemos programar una entrevista para explicarle con más detalle\r\nmi historial profesional, indicar qué ofrezco y poder demostrarles mis capacidades..'),
(7, 'Sara', 'Sánchez Becerra', 'Historiadora Maquetadora Editorial Auxiliar de Biblioteca', 'Particular', 0, 1, 'Medio Cudeyo', 639516663, 'sanchezbe.94@gmail.com', '', '', NULL, '¡Hola! Mi experiencia como maquetadora y auxiliar de biblioteca me ha permitido tomar\r\ncontacto con libros, lectores, escritores o ilustradores, y conocer desde dentro algunos\r\nsecretos del mundo editorial, alimentando mi pasión.', 'Desde que tengo memoria me siento atraída por el mundo del libro. Esta pasión creció tras mi\r\nexperiencia como auxiliar de biblioteca, donde conocí más sobre todo el trabajo que se\r\nesconde detrás de algo tan común (y tan grande) como es un libro. Más tarde, como\r\nmaquetadora, he aprendido sobre los entresijos del mundo editorial, aumentando aún más mi\r\ninterés por este ámbito.\r\nSoy curiosa, metódica y muy ordenada. Estas cualidades, al mezclarse con mi personalidad,\r\nme hacen buscar siempre hacer un buen trabajo y mejorar constantemente.\r\nComo autodidacta, siempre estoy abierta a nuevos proyectos. Por esto, decidí participar en el\r\nequipo creativo de la Lanzadera de Empleo de Medio Cudeyo 2020, encargándome del\r\nmaquetado y artefinal de las tarjetas y folletos para todo nuestro equipo.\r\nEnfocada a la maquetación editorial, busco un hueco allá donde texto e imagen se unan para\r\ntransmitir un mensaje. Libros, trabajos académicos, dossieres, folletos, papelería….cualquier\r\nproyecto que surja en nuestra mente, podemos trabajarlo juntos/as para trasladarlo al papel.\r\nTe invito a concertar una entrevista conmigo para hablar con más detalle sobre mi experiencia y\r\nlas herramientas que puedo poner a tu disposición. '),
(8, 'Regina', 'Huerta Tello', 'Secretaria Administrativa', NULL, 0, 1, 'Medio Cudeyo', 644806830, 'reyihuertatello@gmail.com', '', 'https://www.linkedin.com/in/regina-huerta-78a5211a8/', NULL, 'No puedo volver atrás para cambiar el principio, pero puedo volver a comenzar donde estoy y cambiar el final. Entre las dificultades se esconden las oportunidades...', 'Me he desempeñado en diversas funciones a lo largo de mi vida laboral como: administrativa, recepcionista, almacén de logística, cadena de producción, dependienta, atención al cliente. Soy una persona responsable, honesta, ordenada, trabajadora abierta a nuevos desafíos. Siempre preparada para el cambio, cada circunstancia es única. A lo largo de mi vida he tenido que reinventarme en muchas ocasiones, lo que me ha llevado tener una alta capacidad de adaptación. Mi perfil se puede ajustar a las necesidades actuales y futuras. Cuento con una sólida experiencia laboral. En mi currículum no está todo mi bagaje de mi vida, pero les dejo entre ver mi experiencia laboral..\r\n \r\nUn lugar grato lo hacen las personas que trabajan para lograrlo.'),
(9, 'Nelly', 'Rojas del Castrillo', 'Administración de empresas', NULL, 0, 1, 'Medio Cudeyo', 603476518, 'nrojasdecastrillo@gmail.com', '', 'https://es.linkedin.com/in/nelly-rojas', NULL, 'Me gustan los retos personales, que involucren aprendizaje. Me defino como una persona planificada, organizada con capacidad de análisis. Entre mis competencias están, trabajo en equipo, escucha activa, comunicación efectiva, iniciativa, buenas relaciones interpersonales y me adapto a los cambios con facilidad.', 'La Administración es mi vocación, esta ciencia social y económica con carácter técnico me ha dado grandes satisfacciones profesionales y personales.  Me   he sentido identificada con sus principios desde que comprendí que su finalidad es organizar recursos, gestionarlos y optimizarlos para lograr sus mejores resultados en los diferentes ámbitos empresariales y sociales.\r\nA lo largo de mi trayectoria profesional, he tenido oportunidad de conocer cómo funcionan las empresas del sector financiero (Banca – Seguros), manejando diferentes áreas, tales como contabilidad, Obligaciones Fiscales,Compra, Recursos Humanos, tesorería, gestión de procesos, presupuesto y cobranza. Mis años de trabajo me han formado no solo en el sector económico, también en el social. En las humanidades se necesitan buenos gestores y estrategas para llevar a cabo objetivos.\r\nOfrezco, mi experiencia incondicional, disposición, responsabilidad, motivación y compromiso hacia el trabajo.  Si hay alguna palabra que se haya instalado en el mercado laboral en estos últimos tiempos es la flexibilidad, habilidad que poseo y manejo bien, tanto en el ámbito de ejecución como el trato con las personas.\r\nMe encantaría tener la oportunidad de una entrevista y de esta manera pueda tener una visión más específica de mi perfil y participar  en los procesos de selección de tu empresa.\r\n'),
(10, 'Cristóbal Gabriel', 'Salinas Huerta', 'Administrador de empresas sector logístico, con inglés nivel intermedio', NULL, 0, 1, 'Medio Cudeyo', 644014709, 'cristobalsalinas20@gmail.com', '', 'https://www.linkedin.com/in/cristobal-salinas-huerta-2ba460170/', NULL, 'El éxito no es un accidente, es trabajo duro, perseverancia, aprendizaje, estudio, sacrificio y sobre todo amor por lo que estás haciendo y aprendiendo a hacer. (Pelé)', 'Soy  una persona con una firme actitud para superar todos  los obstáculos, comprometido con el logro de los objetivos personales y laborales, con habilidad para relacionarse, soy amable y servicial, siempre  dispuesto a colaborar y trabajar de manera coordinada con el equipo sin perder de vista la  inquietud por adquirir nuevos  conocimientos  y desarrollar mis  habilidades.\r\nOfrezco mis servicios en el sector de gestión administrativa, con experiencia en logística y un buen nivel de inglés (real).\r\nMi práctica profesional la desarrollé en  Recursos Humanos.'),
(11, 'Juan Ramiro', 'Salinas Olguín', 'Ingeniero Técnico Técnico Superior en Finanzas', NULL, 0, 1, 'Medio Cudeyo', 644208628, 'reju2104@gmail.com', '', 'https://www.linkedin.com/in/juan-salinas-13b63093', NULL, ' “Camina siempre por la vida como si tuvieses algo nuevo que aprender y lo harás.”', 'Cada experiencia laboral  me ha dado la oportunidad de descubrir y desarrollar nuevas habilidades y competencias, capacidad para dirigir y conseguir el apoyo y el compromiso del equipo con los objetivos marcados, compartir conocimientos con objeto de facilitar el desarrollo e integración de las personas, el temple y la entereza para mantener la calma y una buena actitud ante las dificultades, a centrarme en la búsqueda de soluciones, a priorizar,  siempre con apego irrenunciable a mis valores.\r\nSi deciden contar conmigo, no solo van a reclutar a un coordinador de procesos, sino que además, aun motivador y facilitador del desarrollo profesional, convencido de que las oportunidades se generan cuando nos enfocamos en las competencias y habilidades que tengo que desarrollar para hacer un trabajo de excelencia cada día.\r\nEstaré encantado de dar más detalles acerca lo que puedo aportar a vuestra empresa en una entrevista personal.'),
(13, 'Belén', 'Sierra Sierra', 'Dependienta Especializada en Atención al Cliente', NULL, 0, 1, 'Medio Cudeyo', 605220408, 'belensierrasierra@gmail.com', '', 'https://www.linkedin.com/in/belensierrasierra/', NULL, 'Me gusta tratar con personas, considero que todo se puede mejorar cuando el objetivo es ayudar.', 'Hace tiempo fui emprendedora, con mi propia tienda de ropa y complementos, los retos no me asustan. He trabajado en supermercados Carrefour y Grupo Semark, donde fui encargada de sección. Tengo la experiencia necesaria para saber que el trato al cliente es fundamental si queremos que vuelva.\r\nSiempre he trabajado de cara al público, dando lo mejor de mí y atendiendo a los clientes como si fueran parte de mi familia, es un trato que se vuelve recíproco cuando la persona que tienes delante se da cuenta que es diferente, que es especial para ti.\r\nEstoy preparada para desempeñar cualquier función, soy una persona alegre, dinámica y polivalente que se adapta a cualquier situación, siempre con una sonrisa y con un objetivo bien claro ¿te ayudo?'),
(14, 'Margarita', 'Pérez del Río', 'Hostelería (cámara de sala, barra y terraza).  Dependienta, Limpieza  Y Línea de Producción.  Apasionada de la Jardinería y Centros Florales', NULL, 0, 1, 'Medio Cudeyo', 670611563, 'margaperio1971@gmail.com', '', 'https://www.linkedin.com/in/margapérezdelrio/', NULL, '¡Hola!, alegre y con ganas de aprender y aportar en todo lo que hago, me considero una persona optimista, responsable y con mucha capacidad de trabajo.', 'Me gusta el trato con la gente y poder ayudar, por eso parte de mi trayectoria profesional está enfocada hacia la atención al cliente. Cuento con una amplia experiencia principalmente en el sector de la hostelería como camarera de sala, barra y terraza, he prestado servicios para restaurantes con estrella michelín y parador nacional entre otros... Tengo la capacidad de trabajar bajo presión.\r\nSensible y luchadora, me gusta afrontar retos, la vida me enseño que con esfuerzo y trabajo de todo se aprende..., de ahí poder compaginar mi carrera profesional en el sector de la hostelería, con mi pasión de por la jardinería.\r\nPolivalente, estoy abierta a otras ocupaciones, tanto como dependienta, limpieza o en línea de producción.\r\nEstaría encantada de concertar una entrevista y poder contar con más detalle tanto mi trayectoria profesional, como lo que puedo ofrecer y aportar a su empresa.'),
(15, 'Paula', 'Pla Arce', 'Asesoramiento Jurídico y Mediación. Licenciada en Derecho. Núm Colegiada 3992', NULL, 0, 1, 'Medio Cudeyo', 680100335, 'plaarcepaula@gmail.com', '', 'https://www.linkedin.com/in/paulaplaarce/', NULL, 'Hola! me llamo Paula y mi mejor manera de presentarme es diciendo que creo en un mundo justo y conciliador, en el cual cada uno de nosotros debemos colaborar en lo que podamos y con lo que mejor sabemos hacer.', 'Mi entusiasmo por intentar ayudar a los demás, asesorarles acerca de sus dudas y acompañarles durante todo el proceso, hizo que estudiara Derecho y desarrollara mi labor por ese camino.\r\nMi capacidad de escucha activa, me convierte en una entusiasta de la resolución de conflictos en todas sus variantes, siendo mi pasión la mediación. Tanto es así, que hace un tiempo decidí crearme un blog donde plasmo, de forma concisa, temas sobre el mundo del derecho y la mediación (http://miderechoyyo.blogspot.com/)\r\nHe presentado diferentes proyectos de mediación familiar y escolar dirigidos a Administraciones Locales y Centros Educativos en Cantabria con el objetivo de ofrecer recursos para construir buenas relaciones a través de la convivencia.\r\nOfrezco seriedad, responsabilidad, organización y compromiso. Destaco mi habilidad para la redacción, la lectura analítica y facilidad para empatizar y trabajar en equipo con actitud siempre positiva, por encima de todo.., no me gusta ver el vaso medio lleno, prefiero verlo rebosando..  \r\nSerá un placer concertar una entrevista y poder hablar con más detalle a cerca de lo que puedo aportar a la empresa y de las herramientas que pongo a su disposición para continuar  creciendo.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(80) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `tipo` varchar(50) DEFAULT NULL,
  `authKey` varchar(255) DEFAULT NULL,
  `accessToken` varchar(255) DEFAULT NULL,
  `activate` tinyint(4) DEFAULT NULL,
  `verification_code` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `tipo`, `authKey`, `accessToken`, `activate`, `verification_code`) VALUES
(14, 'lanzaderamediocudeyo@gmail.com', 'fsqwOFUzjJdDI', 'lanzaderamediocudeyo@gmail.com', 'administrador', '81fde9ed7b7892872dde9426e1d3dd40d509117dac374c8fc1007a8d93c2115ec2e1d2c5ea7c7c8b74d878c5b3bcf808908370f71526aa9efc7711e0ae84aef4335044c8c4b4177cd2609b92b168038dab8d8f1a5e9d102768e852c0e6aedb728888e065', '6c4cdf1d76470ef17383e5562cdf650f540867e680fa1c4efd0220ebd9c1c5639993d11c80c5c7e744771afd2eb4f4459118bc7b18124199dfa0b054f7686b50866c08c84b545b78cd4ff7c8cbcdc52a088e6b8eb0aa3f84e2c1f00a021937f964e463ec', 1, ''),
(15, 'robherblancalpe@gmail.com', 'fsULW8r.BgWks', 'robherblancalpe@gmail.com', 'administrador', '043785aee1fc3cbc8994d1e63fa8ee4aed9796a5aa86abfb196e113dfbc86012bdac9512a7f6a6ed5aa3d4d2fe2c3c3f1f1e8752322f48796de4eed3bc7c2700e260a52b348a150dd61b3fb53a13433c2b27c379c065551f69aed99de865861fcd1bb603', 'c877bab9ff242270a5d6c03ddae16db0a16ac308403b170d3d8f718df5ae4aca5dd8756617e0dff7128af26f65c42dfdf65522562d8069db2486d50ef1207a1aed94aa6da0fece7ddd36f9d5ae5af04548761f058ece985a39f5e25ca318e08029dc94ad', 1, ''),
(30, 'robherblanc@outlook.com', 'fsULW8r.BgWks', 'robherblanc@outlook.com', 'invitado', '6962b5d60b670f480e294b7767fd5551c19d26dfd3218a625174b0f168eba7310482a4ac6e89f83059bedd8a5863328eb875877f2ae01882e59100cf840f81f9a10c42054839198924ad6469eb63c917f24919c1b5cfe71a1694b8bd77acfb32b6871e7b', '796bf1aa7e697f74aff6187b19a14afe57872bee598d29be210b8e2f95c8eca985062b41b4f3d5c4a82f0098f5d0d5d5e029002b7363bf55d73ac40bfe6d9f3ad6e738bc27a6c66d71709fb592631bab00d2ba9992b753f5f64c6fe749165eb87fdb8ffb', 1, 'a7eebf33');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `preregistro`
--
ALTER TABLE `preregistro`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `talentos`
--
ALTER TABLE `talentos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`,`password`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `preregistro`
--
ALTER TABLE `preregistro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT de la tabla `talentos`
--
ALTER TABLE `talentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
