<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>


<div style="padding: 100px 200px">
    <h3><?= $msg ?></h3>
    <h1>Regístrate</h1>
    <?php $form = ActiveForm::begin([
        'method' => 'post',
     'id' => 'formulario',
     'enableClientValidation' => true,
     'enableAjaxValidation' => false,
    ]);
    ?>
 	
   <div class="form-group">
     <?= $form->field($model, "username")->input("text")->label('Usuario') ?>   
    </div>

    <div class="form-group">
     <?= $form->field($model, "email")->input("email") ?>   
    </div>

    <div class="form-group">
     <?= $form->field($model, "password")->input("password")->label('Contraseña') ?>   
    </div>

    <div class="form-group">
     <?= $form->field($model, "password_repeat")->input("password")->label('Repite Contraseña') ?>   
    </div>

    <?= Html::submitButton("Registrar", ["class" => "btn btn-primary"]) ?>

    <?php  ActiveForm::end(); ?>

</div>